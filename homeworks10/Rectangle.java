package homeworks10;

public class Rectangle extends Figure{

    protected int sideA;
    protected int sideB;

    public Rectangle(int coordinateX, int coordinateY, int sideA, int sideB) {
        super(coordinateX, coordinateY);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public int getSideA() {
        return this.sideA;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }

    public int getSideB() {
        return this.sideB;
    }

    public void setSideB(int sideB) {
        this.sideB = sideB;
    }

    @Override
    public int getPerimeter() {
        return (this.sideA + this.sideB) * 2;
    }
}
