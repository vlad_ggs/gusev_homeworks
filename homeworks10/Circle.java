package homeworks10;

public class Circle extends Ellipse implements Movable{

    public Circle(int coordinateX, int coordinateY, int radius) {
        super(coordinateX, coordinateY, radius, radius);
    }

    @Override
    public int getPerimeter() {
        return (int) (2 * Math.PI * this.radius);
    }

    @Override
    public void changeCoordinates(int x, int y) {
        setCoordinateX(x);
        setCoordinateY(y);
    }

    @Override
    public String toString() {
        return "\n" + "Circle{" +
                ", coordinateX=" + coordinateX +
                ", coordinateY=" + coordinateY +
                "radius=" + radius +
                ", biggerRadius=" + biggerRadius +
                "}";
    }
}
