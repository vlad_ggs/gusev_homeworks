package homeworks10;

public class Ellipse extends Figure{

    protected int radius;
    protected int biggerRadius;

    public Ellipse(int coordinateX, int coordinateY, int radius, int biggerRadius) {
        super(coordinateX, coordinateY);
        this.radius = radius;
        this.biggerRadius = biggerRadius;
    }

    public int getRadius() {
        return this.radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getBiggerRadius() {
        return this.biggerRadius;
    }

    public void setBiggerRadius(int biggerRadius) {
        this.biggerRadius = biggerRadius;
    }


    @Override
    public int getPerimeter() {
        return (int) (4 * ((Math.PI * this.biggerRadius * this.radius) + (Math.pow((this.biggerRadius - this.radius), 2))) / (this.biggerRadius + this.radius));
    }
}
