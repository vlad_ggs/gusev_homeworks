package homeworks10;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        // создал массив "перемещаемых" фигур
        Movable[] movables = new homeworks10.Movable[10];

        // заполнил массив
        for (int i = 0; i < movables.length; i++) {
            if ((1 + (int) (Math.random() * 2)) == 1) movables[i] = new Square((int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10));
            else movables[i] =  new homeworks10.Circle((int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10));
        }
        // изменил координаты
        for (Movable movable : movables) {
            movable.changeCoordinates(5, 10);
        }

        System.out.println(Arrays.toString(movables));
    }
}
