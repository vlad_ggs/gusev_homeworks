package homeworks10;

public class Square extends Rectangle implements Movable{

    public Square(int coordinateX, int coordinateY, int sideA) {
        super(coordinateX, coordinateY, sideA, sideA);
    }

    @Override
    public int getPerimeter() {
        return this.sideA * 4;
    }

    @Override
    public void changeCoordinates(int x, int y) {
        setCoordinateX(x);
        setCoordinateY(y);
    }

    @Override
    public String toString() {
        return "\n" + "Square{" +
                "coordinateX=" + coordinateX +
                ", coordinateY=" + coordinateY +
                ", sideA=" + sideA +
                ", sideB=" + sideB +
                "}";
    }
}
