import java.text.DecimalFormat;

public class HUMAN {

    private String name;
    private double weight;

    public HUMAN(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "HUMAN{" +
                "name='" + name + '\'' +
                ", weight=" + new DecimalFormat("#0.0").format(weight) +
                '}';
    }
}
