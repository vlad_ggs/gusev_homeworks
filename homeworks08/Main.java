import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        HUMAN[] humans = new HUMAN[10];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new HUMAN("Name" + i ,40 + (double) (Math.random() * 80));
        }

        System.out.println(Arrays.toString(humans));
        sortByWeight(humans);
        System.out.println(Arrays.toString(humans));

    }

    public static void sortByWeight(HUMAN[] humans) {
        double minWeight;
        int minIndex;
        for (int i = 0; i < humans.length; i++) {
            minWeight = humans[i].getWeight();
            minIndex = i;
            for (int j = i; j < humans.length; j++) {
                if (minWeight > humans[j].getWeight()) {
                    minWeight = humans[j].getWeight();
                    minIndex = j;
                }
            }
            HUMAN temp = humans[i];
            humans[i] = humans[minIndex];
            humans[minIndex] = temp;
        }
    }
}