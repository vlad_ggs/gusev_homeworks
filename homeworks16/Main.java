package lists;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(33);
        numbers.add(15);
        numbers.add(11);
        numbers.add(89);
        numbers.add(17);
        numbers.add(21);
        numbers.add(89);
        numbers.add(123);
        numbers.add(321);
        numbers.add(83);
        numbers.add(33);
        numbers.add(19);
        numbers.add(42);
        numbers.add(10);
        numbers.add(17);
        numbers.add(8);
        numbers.add(5);
        numbers.add(33);
        numbers.add(15);
        numbers.add(11);
        numbers.add(89);
        numbers.add(17);

        System.out.println(numbers);

        numbers.removeAt(21);
        numbers.removeAt(2222);

        System.out.println(numbers);


        LinkedList<Integer> list = new LinkedList<>();
        list.add(34);
        list.add(120);
        list.add(-10);
        list.add(11);
        list.add(50);
        list.add(100);
        list.add(99);

        list.addToBegin(77);
        list.addToBegin(88);
        list.addToBegin(99);


        System.out.println(list);
        System.out.println(list.get(1));



    }
}
