class Program {
    public class Program {

        public static int getMinimumDigitOfANumber(int[] arr) {
            int number;
            int minDigit = Integer.MAX_VALUE;
            for (int i = 0; arr[i] != -1; i++) {
                if (arr[i] != 0) {
                    number = Math.abs(arr[i]);
                    while (number != 0) {
                        if (number < minDigit) {
                            minDigit = number %= 10;
                            ;
                        }
                        number /= 10;
                    }
                } else minDigit = 0;
            }
            return minDigit;
        }

        public static void main(String[] args) {
            int[] arr = {345, 298, 456, -1};

            System.out.println("Минимальная цифра, встречающаяся среди чисел последовательности - " + getMinimumDigitOfANumber(arr));
        }
    }
}