package homework09;

public class Ellipse extends Figure{
    protected int radius;
    private int biggerRadius;

    public Ellipse(int x, int y, int radius, int biggerRadius) {
        super(x, y);
        this.radius = radius;
        this.biggerRadius = biggerRadius;
    }

    public int getRadius() {
        return radius;
    }

    public int getBiggerRadius() {
        return biggerRadius;
    }

    public int getPerimeter() {
        return (int) (4 * ((Math.PI * this.biggerRadius * this.radius) + (Math.pow((this.biggerRadius - this.radius), 2))) / (this.biggerRadius + this.radius));
    }
}
