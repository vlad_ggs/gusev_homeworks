package homework09;

public class Rectangle extends Figure{
    protected int firstSide;
    private int secondSide;

    public Rectangle(int x, int y, int firstSide, int secondSide) {
        super(x, y);
        this.firstSide = firstSide;
        this.secondSide = secondSide;
    }

    public int getFirstSide() {
        return firstSide;
    }

    public int getSecondSide() {
        return secondSide;
    }

    public int getPerimeter() {
        return (this.firstSide + this.secondSide) * 2;
    }
}
