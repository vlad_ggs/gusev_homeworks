package homework09;

public class Square extends Rectangle{

    public Square(int x, int y, int firstSide) {
        super(x, y, firstSide, firstSide);
    }

    public int getPerimeter() {
        return this.firstSide * 4;
    }
}
