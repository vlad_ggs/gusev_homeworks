package homework09;

public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(1, 0, 3, 9);
        Rectangle rectangle = new Rectangle(0, 1, 5, 6);
        Square square = new Square(1, 0, 7);
        Circle circle = new Circle(1, 0, 10);

        System.out.println(ellipse.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());
        System.out.println(circle.getPerimeter());
    }
}
