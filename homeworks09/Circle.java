package homework09;

public class Circle extends Ellipse{
    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }

    public int getPerimeter() {
        return (int) (2 * Math.PI * this.radius);
    }
}
