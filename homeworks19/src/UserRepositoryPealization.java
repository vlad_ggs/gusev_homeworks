import user.User;
        import user.UsersRepository;

        import java.io.*;
        import java.util.ArrayList;
        import java.util.List;

public class UserRepositoryRealization implements UsersRepository {
    private static final UserRepositoryRealization instance;

    static {
        instance = new UserRepositoryRealization();
    }

    public static UserRepositoryRealization getInstance() {
        return instance;
    }

    @Override
    public List<User> findAll() {
        List<User> allUsers = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader("users.txt");
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (!line.replaceAll("\\s+", "").equals("")) {
                String[] lineParts = line.split("\\|");
                String name = lineParts[0];
                int age = Integer.parseInt(lineParts[1]);
                boolean isWorker = Boolean.parseBoolean(lineParts[2]);
                User newUser = new User(name, age, isWorker);
                allUsers.add(newUser);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {}
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {}
            }
        }
        return allUsers;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter("users.txt", true);
            bufferedWriter = new BufferedWriter(writer);
            String line = user.getName() + "|" + user.getAge() + "|" + user.isWorker();
            bufferedWriter.write(line);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> allUsers = new ArrayList<>(UserRepositoryRealization.getInstance().findAll());
        List<User> usersByAge = new ArrayList<>();
        for (User user : allUsers) {
            if (user.getAge() == age) {
                usersByAge.add(user);
            }
        }
        return usersByAge;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> allUsers = new ArrayList<>(UserRepositoryRealization.getInstance().findAll());
        List<User> workers = new ArrayList<>();
        for (User user : allUsers) {
            if (user.isWorker()) {
                workers.add(user);
            }
        }
        return workers;
    }
}
